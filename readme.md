Setting Up
========================

## Author(s)
*   Romain Julien
*   Lisa Liu-Thorrold

--------------------------------------------------------------------------------
## Getting started
##### These instructions are for setup using the Intellij IDE 
1. Clone the repository from the following location

    https://lisa_liu-thorrold@bitbucket.org/julienandliu/thesis-nodejs.git
2. Make sure you have node and npm installed
    `brew install node`

3. Install the required packages for the project (will install the package in a node_modules folder in the parent working directory).
 `npm install`

4. Start the node js server
`npm start`

When you issue the command `npm start` from the root directory of your nodejs project, node will look for a scripts (start) object in your package.json file. If found, it will look for a script with the key start and run the command specified as its value. If your package.json does not have any scripts object or if the scripts object does not have a start key, it will run the command node server.js instead.