/**
 * Created by romainjulien on 29/07/2016.
 */
var elementIdHolder = {};

var constructor = {

    getProjectStructure: function(projectPath, projectId) {
        
        var struct = [
            {
                _id: elementIdHolder[projectId].classpath,
                name: ".classpath",
                path: '/.classpath',
                type: "metadata",
                packageName: null
            },
            {
                _id: elementIdHolder[projectId].project,
                name: ".project",
                path: '/.project',
                type: "metadata",
                packageName: null
            },
            {
                _id: elementIdHolder[projectId].bin,
                name: "bin",
                path: '/bin',
                type: "folder",
                packageName: null,
                children: []
            },
            {
                _id: elementIdHolder[projectId].src,
                name: "src",
                path: '/src',
                type: "folder",
                packageName: '',
                children: []
            }
        ];
        
        delete elementIdHolder[projectId];
        
        return struct;
    },
    
    storeProjectElementId: function(projectId, elementName, elementId) {

        if (elementIdHolder[projectId] == undefined) {
            elementIdHolder[projectId] = {};
        }

        switch (elementName) {
            case '.classpath':
                elementIdHolder[projectId].classpath = elementId.toString();
                break;
            case '.project':
                elementIdHolder[projectId].project = elementId.toString();
                break;
            case 'bin':
                elementIdHolder[projectId].bin = elementId.toString();
                break;
            case 'src':
                elementIdHolder[projectId].src = elementId.toString();
                break;
            default:
                break;
        }
    }
    
};

exports.constructor = constructor;
