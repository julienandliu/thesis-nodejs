/**
 * Created by romainjulien on 27/07/2016.
 */
var net = require('net');
var sm = require('../connection/systemManagement');
var pm = require('../connection/projectManagement');
var rt = require('../realTime/realTime');

var eclipseSocket;
var pendingRequests = {};

var init = function(ip, port) {
    eclipseSocket = new net.Socket();

    eclipseSocket.on('error', function(data) {
        console.log('Eclipse socket - ' + data);
    });
    
    eclipseSocket.connect(port, ip, function() {
        console.log('Connected to Eclipse plugin');
        eclipseSocket.on('data', callbackOnData);
        eclipseSocket.on('close', callbackOnClose);
    });
};

var callbackOnData = function(msg) {
    var msgJson = JSON.parse(msg);
    console.log('Received from Eclipse plugin: ' + JSON.stringify(msgJson));

    switch (pendingRequests[msgJson.requestID]) {
        case 'createProject':
            sm.handleResponse.createProject(msgJson);
            break;
        case 'deleteProject':
            sm.handleResponse.deleteProject(msgJson);
            break;
        case 'createPackage':
            pm.handleResponse.createPackage(msgJson);
            break;
        case 'deletePackage':
            pm.handleResponse.deletePackage(msgJson);
            break;
        case 'createClass':
            pm.handleResponse.createClass(msgJson);
            break;
        case 'deleteClass':
            pm.handleResponse.deleteClass(msgJson);
            break;
        case 'compileProject':
            rt.handleResponse.compilation(msgJson);
            break;
        default:
            break;
    }
    
    delete pendingRequests[msgJson.requestID];
};

var callbackOnClose = function() {
    console.log('Connection closed');
};

var systemManagement = {
    createProject: function(projectName, requestID) {
        var createProjectJson = {
            requestID: requestID,
            commandName: 'create',
            objectType: 'project',
            objectName: projectName,
            additionalParameters: []
        };
        pendingRequests[requestID] = 'createProject';
        eclipseSocket.write(JSON.stringify(createProjectJson) + '\n');
        console.log("Sent to eclipse: " + JSON.stringify(createProjectJson));
    },

    deleteProject: function(projectName, requestID) {
        var deleteProjectJson = {
            requestID: requestID,
            commandName: 'delete',
            objectType: 'project',
            objectName: projectName,
            additionalParameters: []
        };
        pendingRequests[requestID] = 'deleteProject';
        eclipseSocket.write(JSON.stringify(deleteProjectJson) + '\n');
    }    
};

var projectManagement = {
    createPackage: function(projectName, packageName, requestID) {
        var createPackageJson = {
            requestID: requestID,
            commandName: "create",
            objectType: "package",
            objectName: packageName,
            additionalParameters: [projectName]
        };
        pendingRequests[requestID] = 'createPackage';
        eclipseSocket.write(JSON.stringify(createPackageJson) + '\n');
    },

    deletePackage: function(projectName, packageName, requestID) {
        var deletePackageJson = {
            requestID: requestID,
            commandName: "delete",
            objectType: "package",
            objectName: packageName,
            additionalParameters: [projectName]
        };
        pendingRequests[requestID] = 'deletePackage';
        eclipseSocket.write(JSON.stringify(deletePackageJson) + '\n');
    },

    createClass: function(projectName, packageName, className, requestID) {
        var createClassJson = {
            requestID: requestID,
            commandName: "create",
            objectType: "class",
            objectName: className,
            additionalParameters: [projectName, packageName]
        };
        pendingRequests[requestID] = 'createClass';
        eclipseSocket.write(JSON.stringify(createClassJson) + '\n');
    },

    deleteClass: function(projectName, packageName, className, requestID) {
        var deleteClassJson = {
            requestID: requestID,
            commandName: "delete",
            objectType: "class",
            objectName: className,
            additionalParameters: [projectName, packageName]
        };
        pendingRequests[requestID] = 'deleteClass';
        eclipseSocket.write(JSON.stringify(deleteClassJson) + '\n');
    }

};

var compilation = {

    compileProject: function(projectName, requestID) {

        var compileProjectJson = {
            requestID: requestID,
            commandName: "compile",
            objectType: "project",
            objectName: projectName,
            additionalParameters: []
        };

        pendingRequests[requestID] = 'compileProject';
        eclipseSocket.write(JSON.stringify(compileProjectJson) + '\n');
    }
};

exports.initConnection = init;
exports.systemManagement = systemManagement;
exports.projectManagement = projectManagement;
exports.compilation = compilation;