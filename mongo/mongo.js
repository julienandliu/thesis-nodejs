/**
 * Created by romainjulien on 29/07/2016.
 */
var MongoClient = require('mongodb').MongoClient;
var db;

var init = function(ip, port, dbName) {
    var url = 'mongodb://'+ ip + ':' + port + '/' + dbName;
    MongoClient.connect(url, function(err, database) {
        if (err == null) {
            db = database;
            exports.db = db;
            console.log("Connected to mongo");
        } else {
            console.log("Error connecting to mongo - " + err);
        }
        //database.close();
    });
};

exports.init = init;
exports.db = db;
