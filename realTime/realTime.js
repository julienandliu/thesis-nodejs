/**
 * Created by romainjulien on 2/08/2016.
 */
var ot = require('ot');
var fs = require('fs');

var eclipse = require('../eclipse/eclipse');
var controller = require('../connection/synchronisationController');
var common = require('../common/common');
var mongo = require('../mongo/mongo');

var pendingRequest = {};
// Can be IDLE, PENDING, QUEUING
var projectCompilationState = {};

var jobWorker = {

    onReceiveOperation: function(msgJson, client) {

        console.log('received operation: ' + JSON.stringify(msgJson.operation));

        var operation = ot.TextOperation.fromJSON(msgJson.operation);
        var fileConnection = controller.utils.getConnectionForFile(msgJson.fileId);

        var server = fileConnection.server;

        // Return the transformed (or original if no transformation necessary) operation and UPDATE server.document
        operation = server.receiveOperation(msgJson.revision, operation);

        // Write the new version to file
        persistChanges(msgJson.projectId, msgJson.fileId, server.document);

        // Increment revision number in db
        // and Save operations history on this file
        var collection = mongo.db.collection('projectElements');
        collection.findOneAndUpdate(
            {_id: msgJson.fileId},
            {$inc: {revision: 1}, $set: {operations: server.operations}},
            {upsert: true, returnOriginal: false}
        );

        // Broadcast operation to users
        var connectedUsers = fileConnection.users;
        connectedUsers.forEach(function(item, index) {
            var op = {
                action: 'operation',
                userId: msgJson.userId,
                fileId: msgJson.fileId,
                operation: operation
            };
            
            var connection = controller.utils.getClientConnectionForUserId(item);
            if (connection != undefined) {
                console.log("sending " + JSON.stringify(op) + " to user id: " + item);
                connection.send(JSON.stringify(op));
            } else {
                console.log("A client has crashed, user id: " + item + " closing up his connection");
                controller.utils.cleanUpConnectionForLostConnection(msgJson.projectId, item);
            }
        });


        var allText = [];
        var compile = false;
        for (var i in operation.ops) {
            var op = operation.ops[i];

            if (ot.TextOperation.isInsert(op)) {
                if (op == undefined || !isNaN(op) || op.match(/[\s;}]/)) {
                    compile = true;
                }

                allText.push(op);
            }
        }

        console.log('Character received: ' + allText.toString());

        if (compile) {
            console.log("YES COMPILE");
            // Send compilation request to eclipse
            requestCompilation(msgJson.projectId);
        } else {
            console.log("NO DONT COMPILE");
        }


    }

};

var compilation = {

    reqCompilation: function(projectId) {
        requestCompilation(projectId);
    }

};

function persistChanges(projectId, fileId, document) {
    common.mongoOperations.getProject(projectId
    ).then(function(projects) {
        var pathProject = projects[0].pathToProjectFolder;

        common.mongoOperations.getProjectElement(fileId
        ).then(function(elements) {
            var pathElement = elements[0].path;
            var filePath = pathProject + pathElement;

            fs.writeFile(filePath, document, function(err) {
                if (err) {
                    console.log(err);
                }
            });

        }).catch(function(err) {
            var comment = 'Error getting file from mongo, fileId: ' + fileId;
            console.log(comment);
            console.log(err);
        });
    }).catch(function(err) {
        var comment = 'Error getting project from mongo, projectId: ' + projectId;
        console.log(comment);
        console.log(err);
    });
}

/**
 * Check the state of the project before sending a compilation request, the project can be in one of three states:
 * - IDLE: ready to send request, state -> PENDING
 * - PENDING: a request has been sent, no response yet, state -> QUEUING
 * - QUEUING: when the compilation response is received, send a new request immediately, state -> PENDING
 *
 * The state will become IDLE again if when the response is received, the state is PENDING (and not QUEUING)
 *
 * @param projectId
 */
function requestCompilation(projectId) {
    console.log('request compilation, state: ' + projectCompilationState[projectId]);
    if (projectCompilationState[projectId] == undefined) {
        projectCompilationState[projectId] = 'IDLE';
    }

    switch (projectCompilationState[projectId]) {
        case 'IDLE':
            var requestID = getRandomrequestID();
            pendingRequest[requestID] = projectId;
            eclipse.compilation.compileProject(projectId, requestID);
            projectCompilationState[projectId] = 'PENDING';
            break;
        case 'PENDING':
            projectCompilationState[projectId] = 'QUEUING';
            break;
        case 'QUEUING':
            // do nothing
            break;
        default:
            break;
    }
}

var handleResponse = {

    compilation: function(response) {

        var projectId = pendingRequest[response.requestID];

        if (projectCompilationState[projectId] == 'QUEUING') {
            console.log("handleResponse.compilation - QUEUING - send another request, now PENDING");
            projectCompilationState[projectId] = 'PENDING';
            var requestID = getRandomrequestID();
            pendingRequest[requestID] = projectId;
            eclipse.compilation.compileProject(projectId, requestID);
        } else if (projectCompilationState[projectId] == 'PENDING') {
            console.log("handleResponse.compilation - PENDING - now IDLE");
            projectCompilationState[projectId] = 'IDLE';
        }

        delete pendingRequest[response.requestID];

        if (response.responseCode != 200) {
            console.log("Error trying to compile project " + projectId + " : " + response.responseComment);
            return;
        }

        // no errors doesn't trigger anything
        if (response.compilationErrors.length == 0) {
            console.log("NO compilation errors");
            // return;
        }

        var compilationErrorsPlugin = {};
        // Extract all different files with errors
        // Note that to uniquely identify the files, we look for its path relative to the project root
        response.compilationErrors.forEach(function(item, index) {

            var path = item.fileID;
            if (!path.match(/^\//)) {
                path = "/" + item.fileID; // Lisa forgot to rename it
            }


            if (compilationErrorsPlugin[path] == undefined) {
                compilationErrorsPlugin[path] = [item];
            } else {
                compilationErrorsPlugin[path].push(item);
            }
        });


        var compilationErrors = {};
        // For each file, get his id and store the error in a dictionary id -> [array compilation errors]
        var promises = Object.keys(compilationErrorsPlugin).map(function(path) {

            return new Promise(function(resolve, reject) {

                return common.mongoOperations.getProjectElementForPath(projectId, path
                ).then(function (docs) {

                    var fileId = docs[0]._id;
                    var path = docs[0].path;
                    compilationErrors[fileId] = compilationErrorsPlugin[path];

                    resolve();

                }).catch(function (err) {
                    reject();
                    console.log("Error getting file given its path in project: " + projectId);
                    console.log("Project path: " + path);
                    console.log("Err")
                });
            });

        });

        Promise.all(promises).then(function(res) {
            // Broadcast operation to users connected to the project
            var connectedUsers = controller.utils.getConnectionsForProject(projectId);
            connectedUsers.forEach(function(item, index) {

                var compilationJson = {
                    action: 'compilation',
                    projectId: projectId,
                    compilationErrors: compilationErrors
                };

                var connection = controller.utils.getClientConnectionForUserId(item);
                if (connection != undefined) {
                    console.log("sending " + JSON.stringify(compilationJson) + " to user id: " + item);
                    connection.send(JSON.stringify(compilationJson));
                } else {
                    console.log("A client has crashed, user id: " + item + " closing up his connection");
                    controller.utils.cleanUpConnectionForLostConnection(projectId, item);
                }

            });
        }).catch(function(err) {
            console.log(err);
        });
    }
};


function getRandomrequestID() {
    return Math.random().toString(36).substring(2);
}

exports.jobWorker = jobWorker;
exports.compilation = compilation;
exports.handleResponse = handleResponse;