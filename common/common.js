/**
 * Created by romainjulien on 21/08/2016.
 */
var mongo = require('../mongo/mongo');

var mongoOperations = {

    getProject: function(projectId) {
        var collection = mongo.db.collection('projects');

        return collection.find(
            {_id: projectId}
        ).limit(1).toArray();
    },

    getProjectElement: function (fileId) {
        var collection = mongo.db.collection('projectElements');
    
        return collection.find(
            {_id: fileId}
        ).limit(1).toArray();
    },

    getProjectElementForPath: function(projectId, path) {
        var collection = mongo.db.collection('projectElements');

        return collection.find(
            {projectId: projectId, path: path}
        ).limit(1).toArray();
    },

    getUser: function(userId) {
        var collection = mongo.db.collection('users');

        return collection.find(
            {_id: userId}
        ).limit(1).toArray();
    }
    
};

exports.mongoOperations = mongoOperations;