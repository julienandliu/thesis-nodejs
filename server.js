var systemManagement = require('./connection/systemManagement');
var projectManagement = require('./connection/projectManagement');
var synchronisationController = require('./connection/synchronisationController');
var eclipsePlugin = require('./eclipse/eclipse');
var mongodb = require('./mongo/mongo');

//Create client socket to connect to eclipse server -> connect to port 9001 which eclipse server is listening on
eclipsePlugin.initConnection('127.0.0.1', 9000);

// Make a new websocket to listen to requests from clients logged in on the home page (system management).
// This websocket is the entry point to the application to create/modify/delete projects only.
systemManagement.initServer(8083);

// Make a new websocket to listen to requests from clients logged in on the IDE page.
// This websocket will listen to requests to create/modify/delete classes only.
projectManagement.initServer(8084);

// Make a new websocket to listen to requests from clients logged in on the IDE page.
// This websocket is used to receive file edits made by users. Upon receiving a change, the
// real-time synchronisation mechanism will take place.
synchronisationController.initServer(8085);

//Connect to mongo database
mongodb.init('localhost', '27017', 'testdb');