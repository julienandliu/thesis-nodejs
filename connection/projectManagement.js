/**
 * Encapsulate the server socket used to received requests to create/modify/delete a project element (class, package etc)
 * Created by romainjulien on 27/07/2016.
 */
var fs = require('fs');
var WSS = require('ws').Server;
var eclipse = require('../eclipse/eclipse');
var sc = require('./synchronisationController');
var mongo = require('../mongo/mongo');
var common = require('../common/common');

var server;
var pendingRequest = {};

var initServer = function(port) {
    server = new WSS({ port: port });
    console.log('Project Management listening on port ' + port);

    server.on('error', function(data) {
        console.log('Project Management - ' + data);
    });

    server.on('connection', function(client) {
        client.on('message', callBackOnMessage.bind(this, client));
        client.on('close', callbackOnClose);
    });
};

var callBackOnMessage = function(client, msg) {
    console.log('Received Project Management - ' + msg);
    var msgJson = JSON.parse(msg);

    switch (msgJson.action) {
        case 'getFileContent':
            handleRequest.getFileContent(client, msgJson);
            break;
        case 'createPackage':
            handleRequest.createPackage(client, msgJson);
            break;
        case 'createClass':
            handleRequest.createClass(client, msgJson);
            break;
        case 'deleteClass':
            handleRequest.deleteClass(client, msgJson);
            break;
        default:
            break;
    }
};

var callbackOnClose = function() {
    console.log("Project Management - connection closed");
};

var handleRequest = {

    getFileContent: function (client, msgJson) {
        var userId = msgJson.userId;
        var projectId = msgJson.projectId;
        var fileId = msgJson.fileId;

        var fileConnection = sc.utils.getConnectionForFile(fileId);
        
        if (fileConnection != undefined) {

            sc.utils.addUserToFileConnections(fileId, userId, server);

            sendFileContentToClient(client, msgJson.requestID, fileId, fileConnection.server.document);
            return;
        }
        
        common.mongoOperations.getProject(projectId
        ).then(function(projects) {
            var pathProject = projects[0].pathToProjectFolder;
            
            common.mongoOperations.getProjectElement(fileId
            ).then(function(elements) {
                var pathElement = elements[0].path;
                var filePath = pathProject + pathElement;

                fs.readFile(filePath, 'utf8', function(err, data) {
                    if (err) {
                        var comment = "Error retrieving file content: " + err;
                        sendErrorToClient(client, msgJson.requestID, comment);
                        return;
                    }

                    sc.utils.newFileConnection(fileId, userId, data);

                    sendFileContentToClient(client, msgJson.requestID, fileId, data);
                });

            }).catch(function(err) {
                var comment = 'Error getting file from mongo, fileId: ' + fileId;
                console.log(comment);
                console.log(err);
                sendErrorToClient(client, msgJson.requestID, comment);
            });
        }).catch(function(err) {
            var comment = 'Error getting project from mongo, projectId: ' + projectId;
            console.log(comment);
            console.log(err);
            sendErrorToClient(client, msgJson.requestID, comment);
        });

    },

    createPackage: function(client, msgJson) {
        pendingRequest[msgJson.requestID] = {msg: msgJson, client: client};

        var packageName = msgJson.packageName;
        var newPackageName = msgJson.name;

        if (packageName == '') {
            packageName += newPackageName;
        } else {
            packageName += '.' + newPackageName;
        }

        eclipse.projectManagement.createPackage(msgJson.projectId, packageName,
            msgJson.requestID);
    },

    createClass: function(client, msgJson) {
        pendingRequest[msgJson.requestID] = {msg: msgJson, client: client};
        eclipse.projectManagement.createClass(msgJson.projectId, msgJson.packageName,
            msgJson.name, msgJson.requestID);
    },

    deleteClass: function(client, msgJson) {
        pendingRequest[msgJson.requestID] = {msg: msgJson, client: client};
        eclipse.projectManagement.deleteClass(msgJson.projectName, msgJson.packageName,
            msgJson.fileName, msgJson.requestID);
    }

};

/**
 * These methods handle the response sent back from the Eclipse plugin after a request.
 * @param response
 */
var handleResponse = {

    createPackage: function (response) {

        if (response.responseCode != 200) {
            sendErrorToClient(pendingRequest[response.requestID].client, response.requestID, response.responseComment);
            return;
        }

        var elementName = pendingRequest[response.requestID].msg.name;
        var packageName = pendingRequest[response.requestID].msg.packageName;

        if (packageName == '') {
            packageName += elementName;
        } else {
            packageName += '.' + elementName;
        }

        createElement(response, elementName, packageName, 'package', []);
    },

    deletePackage: function(response) {

        if (response.responseCode != 200) {
            sendErrorToClient(pendingRequest[response.requestID].client, response.requestID, response.responseComment);
            return;
        }

        //TODO 
    },
    
    createClass: function (response) {

        if (response.responseCode != 200) {
            sendErrorToClient(pendingRequest[response.requestID].client, response.requestID, response.responseComment);
            return;
        }

        var elementName = pendingRequest[response.requestID].msg.name;
        var packageName = pendingRequest[response.requestID].msg.packageName;
        createElement(response, elementName, packageName, 'class', null);
    },

    deleteClass: function(response) {

        if (response.responseCode != 200) {
            sendErrorToClient(pendingRequest[response.requestID].client, response.requestID, response.responseComment);
            return;
        }

        //TODO delete file from db
    }
};

function createElement(response, elementName, packageName, type, children) {

    var userId = Number(pendingRequest[response.requestID].msg.userId);
    var projectId = Number(pendingRequest[response.requestID].msg.projectId);

    // Get project (structure) from mongo
    common.mongoOperations.getProject(projectId
    ).then(function(docs) {

        var projectStructure = docs[0].projectStructure;
        var pathArray = response.responseContent.split('/');
        var indexRoot = pathArray.indexOf(projectId.toString());

        // Iterate through project structure to find parent folder of the new class
        var parentJson = DFSProjectStructure(projectStructure, pathArray, indexRoot);

        getNextElementId(
        ).then(function(r) {

            // Add new json element to project structure
            var elementId = r.value.counter;

            pathArray.splice(0, indexRoot + 1);
            var path = "";
            pathArray.forEach(function(element, index) {
                path += "/" + element;
            });

            replaceProjectStructure(projectStructure, parentJson, projectId, elementId, elementName, path,
                type, packageName, children
            ).then(function() {

                saveElementInDatabase(projectId, elementId, elementName, type, path, packageName
                ).then(function() {

                    //Send Response back to client
                    var toSendSuccess = {
                        requestID: response.requestID,
                        responseCode: response.responseCode,
                        responseComment: path,
                        fileId: elementId
                    };

                    sendBackToClient(pendingRequest[response.requestID].client, toSendSuccess);

                    // Send other collaborators connected a notification to update the project structure
                    sendUpdateNotificationToCollaborators(projectId, userId);

                }).catch(function(err) {
                    console.log('projectManagement error: ' + err);
                    var msg = "Error inserting new " + type + " in database";
                    sendErrorToClient(pendingRequest[response.requestID].client, response.requestID, msg);
                });
            }).catch(function(err) {
                console.log('projectManagement error: ' + err);
                var msg = "Error updtating project structure in database";
                sendErrorToClient(pendingRequest[response.requestID].client, response.requestID, msg);
            });
        }).catch(function(err) {
            console.log('projectManagement error: ' + err);
            var msg = "Error getting new " + type + " id";
            sendErrorToClient(pendingRequest[response.requestID].client, response.requestID, msg);
        });
    }).catch(function(err) {
        console.log('projectManagement error: ' + err);
        var msg = "Error getting projects info from database";
        sendErrorToClient(pendingRequest[response.requestID].client, response.requestID, msg);
    });
}

var getNextElementId = function() {
    var collection = mongo.db.collection('counterId');

    return collection.findOneAndUpdate(
        {_id: 'projectElement'},
        {$inc: {counter: 1}},
        {upsert: true, returnOriginal: false}
    );
};

var replaceProjectStructure = function(projectStructure, parentJson, projectId, elementId, elementName,
                                       path, type, packageName, children) {

    var newElement = {
        _id: elementId,
        name: elementName,
        path: path,
        type: type,
        packageName: packageName,
        children: children
    };

    parentJson.push(newElement);

    // Replace updated project structure
    var collection = mongo.db.collection('projects');
    return collection.findOneAndUpdate(
        {_id: projectId},
        {$set: {projectStructure: projectStructure}}
    );
};

var saveElementInDatabase = function(projectId, elementId, elementName, type, path, packageName) {

    var newElement = {
        _id: elementId,
        projectId: projectId,
        name: elementName,
        type: type,
        path: path,
        packageName: packageName,
        revision: 0
    };

    var collection = mongo.db.collection('projectElements');

    return collection.insertOne(
        newElement
    );
};

function DFSProjectStructure(arrayElements, pathArray, index) {
    if (index == pathArray.length - 2) {
        return arrayElements;
    }

    for (var j in arrayElements) {
        if (arrayElements[j].name == pathArray[index + 1]) {
            return DFSProjectStructure(arrayElements[j].children, pathArray, index + 1);
        }
    }
}

function sendFileContentToClient(client, requestID, fileId, content) {

    common.mongoOperations.getProjectElement(fileId
    ).then(function(docs) {
        var revision = docs[0].revision;

        var toSendSuccess = {
            requestID: requestID,
            responseCode: 200,
            fileId: fileId,
            content: content,
            revision: revision
        };

        sendBackToClient(client, toSendSuccess);
    });
}

function sendUpdateNotificationToCollaborators(projectId, userId) {

    common.mongoOperations.getUser(userId
    ).then(function(docs) {
        var username = docs[0].firstName + " " + docs[0].lastName;

        var updateJson = {
            action: 'notification',
            type: 'projectStructureUpdate',
            byUser: username
        };

        var collaboratorsConnected = sc.utils.getCollaboratorsConnected(projectId, userId);

        collaboratorsConnected.forEach(function(collaboratorId, index) {

            var connection = utils.getClientConnectionForUserId(collaboratorId);
            if (connection != undefined) {
                console.log("sending " + JSON.stringify(updateJson) + " to user id: " + collaboratorId);
                connection.send(JSON.stringify(updateJson));
            } else {
                console.log("A client has crashed, user id: " + collaboratorId + " closing up his connection");
                utils.cleanUpConnectionForLostConnection(projectId, collaboratorId);
            }
        });
    });
}

function sendBackToClient(client, msgJson) {
    console.log("Project management - Sending back to client: " + JSON.stringify(msgJson));
    client.send(JSON.stringify(msgJson));
    delete pendingRequest[msgJson.requestID];
}

function sendErrorToClient(client, requestId, errorMessage) {
    var toSendError = {
        requestID: requestId,
        responseCode: 400,
        responseComment: errorMessage
    };

    console.log("Project management - Sending error to client: " + JSON.stringify(toSendError));

    client.send(JSON.stringify(toSendError));
    delete pendingRequest[response.requestID];
}

exports.initServer = initServer;
exports.handleResponse = handleResponse;