/**
 * Encapsulate the server socket used to received requests to create/modify/delete a project.
 * Created by romainjulien on 27/07/2016.
 */
var WSS = require('ws').Server;
var eclipse = require('../eclipse/eclipse');
var mongo = require('../mongo/mongo');
var projectStructure = require('../projectStructure/projectStructure');

var server;
var pendingRequest = {};

var initServer = function (port) {
    server = new WSS({ port: port });
    console.log('System Management listening on port ' + port);

    server.on('error', function(data) {
        console.log('System Management - ' + data);
    });

    server.on('connection', function(client) {
        client.on('message', callBackOnMessage.bind(this, client));
        client.on('close', callbackOnClose);
    });
};

var callBackOnMessage = function(client, msg) {
    console.log("System management - Received - " + msg);
    var msgJson = JSON.parse(msg);

    switch (msgJson.action) {
        case 'createProject':
            handleRequest.createProject(client, msgJson);
            break;
        case 'deleteProject':
            handleRequest.deleteProject(client, msgJson);
            break;
        default:
            break;
    }
};

var callbackOnClose = function() {
    console.log("System Management - connection closed");
};

var handleRequest = {

    /**
     * Get the next projectId then send a create project request to the eclipse plugin with the id as a name.
     *
     * @param client Connection with the client
     * @param msgJson in the form
     *      action: 'createProject',
     *      requestID: requestID,
     *      userId: userId,
     *      name: projectName
     */
    createProject: function(client, msgJson) {

        // Get the next project Id
        var collection = mongo.db.collection('counterId');
        collection.findOneAndUpdate(
            {_id: 'project'},
            {$inc: {counter: 1}},
            {upsert: true, returnOriginal: false}

        ).then(function(r) {

            msgJson.projectId = r.value.counter;
            pendingRequest[msgJson.requestID] = {msg: msgJson, client: client};
            eclipse.systemManagement.createProject(msgJson.projectId, msgJson.requestID);

        }).catch(function(err) {

            var comment = "Could not create project " + msgJson.name + " (Issue retrieving projectId)";

            var toSendErrorCounterId = {
                requestID: msgJson.requestID,
                responseCode: 400,
                responseComment: comment
            };

            sendBackToClient(client, toSendErrorCounterId);

        });
    },

    deleteProject: function(client, msgJson) {

        pendingRequest[msgJson.requestID] = {msg: msgJson, client: client};
        eclipse.systemManagement.deleteProject(msg.name, msgJson.requestID);
    }
};

/**
 * These methods handle the response sent back from the Eclipse plugin after a create/modify/delete Project request.
 * @param response
 */
var handleResponse = {
    
    createProject: function(response) {
        
        if (response.responseCode != 200) {
            var toSendErrorEclipse = {
                requestID: response.requestID,
                responseCode: response.responseCode,
                responseComment: response.responseComment
            };
            sendBackToClient(pendingRequest[response.requestID].client, toSendErrorEclipse);
            return;
        }

        var projectId = pendingRequest[response.requestID].msg.projectId;
        var projectName = pendingRequest[response.requestID].msg.name;
        var userId = Number(pendingRequest[response.requestID].msg.userId);
        var pathToProject = response.responseContent;

        getNextId('projectElement', null).then(
            saveProjectElement.bind(this, projectId, '.classpath', 'metadata', '/.classpath', null)
        ).then(
            getNextId.bind(this, 'projectElement')
        ).then(
            saveProjectElement.bind(this, projectId, '.project', 'metadata', '/.project', null)
        ).then(
            getNextId.bind(this, 'projectElement')
        ).then(
            saveProjectElement.bind(this, projectId, 'bin', 'folder', '/bin', null)
        ).then(
            getNextId.bind(this, 'projectElement')
        ).then(
            saveProjectElement.bind(this, projectId, 'src', 'package', '/src', '')
        ).then(
            saveProject.bind(this, projectId, projectName, userId, pathToProject)
        ).then(function(r) {

            //Send Response back to client
            var toSendSuccess = {
                requestID: response.requestID,
                responseCode: response.responseCode,
                responseComment: response.responseComment
            };

            sendBackToClient(pendingRequest[response.requestID].client, toSendSuccess);

            }).catch(function(err) {

            console.log("ERROR: " + err);

            var comment = "Could not save project " +
                pendingRequest[response.requestID].msg.name + " in database.";

            var toSendErrorInsert = {
                requestID: response.requestID,
                responseCode: response.responseCode,
                responseComment: comment
            };

            sendBackToClient(pendingRequest[response.requestID].client, toSendErrorInsert);
        });
    },

    deleteProject: function(response) {
        
        if (response.responseCode != 200) {
            var toSendErrorEclipse = {
                requestID: response.requestID,
                responseCode: response.responseCode,
                responseComment: response.responseComment
            };
            sendBackToClient(pendingRequest[response.requestID].client, toSendErrorEclipse);
            return;
        }
        
        // TODO
    }
};

var saveProject = function(projectId, projectName, userId, pathToProject) {

    // Create the project document and save in Mongo
    var newProject = {
        _id: projectId,
        _class: 'SystemManagement.Entity.ProjectDocument',
        projectName: projectName,
        ownerId: userId,
        createdDate: new Date(),
        pathToProjectFolder: pathToProject,
        collaborators: null,
        projectStructure: projectStructure.constructor.getProjectStructure(pathToProject, projectId)
    };

    var collection = mongo.db.collection('projects');
    return collection.insertOne(
        newProject
    );
};

var getNextId = function(sequence, r) {

    var collection = mongo.db.collection('counterId');

    return collection.findOneAndUpdate(
        {_id: sequence},
        {$inc: {counter: 1}},
        {upsert: true, returnOriginal: false}
    );
};

var saveProjectElement = function(projectId, name, type, path, packageName, r) {
    var elementId = r.value.counter;

    projectStructure.constructor.storeProjectElementId(projectId, name, elementId);

    var newProjectElement = {
        _id: elementId,
        projectId: projectId,
        name: name,
        type: type,
        path: path,
        packageName: packageName,
        revision: 0
    };

    var collection = mongo.db.collection('projectElements');

    return collection.insertOne(
        newProjectElement
    );
};

function sendBackToClient(client, msgJson) {
    console.log("System management - Sending back to client: " + JSON.stringify(msgJson));
    client.send(JSON.stringify(msgJson));
    delete pendingRequest[msgJson.requestID];
}

exports.initServer = initServer;
exports.handleResponse = handleResponse;
