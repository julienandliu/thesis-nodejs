/**
 * Encapsulate the server socket used to received changes made by the user.
 * This is the entry point of the synchronisation mechanism.
 * This class keeps track of connected users.
 * Created by romainjulien on 27/07/2016.
 */

var ot = require('ot');
var fs = require('fs');
var WSS = require('ws').Server;
var eclipse = require('../eclipse/eclipse');
var mongo = require('../mongo/mongo');
var rt = require('../realTime/realTime');
var common = require('../common/common');

var server;

// The connection pool keeps a socket connection entry for each user currently connected
var connectionPool = {};
// Keeps track of all collaborators currently working on a project
var projectConnections = {};
// Keeps track of all collaborators currently working on a file
var fileConnections = {};
// Keeps track of all collaborators active file
var collaboratorsActiveFile = {};

var initServer = function (port) {
    server = new WSS({ port: port });
    console.log('Synchronisation controller listening on port ' + port);

    server.on('error', function(data) {
        console.log('Synchronisation Controller - ' + data);
    });

    server.on('connection', function(client) {
        client.on('message', callBackOnMessage.bind(this, client));
        client.on('close', callbackOnClose);
    });
};

var callBackOnMessage = function(client, msg) {
    console.log('Received Sync controller - ' + msg);

    var msgJson = JSON.parse(msg);

    switch (msgJson.action) {
        case 'initConnection':
            handleRequest.initConnection(client, msgJson);
            break;
        case 'closeConnection':
            handleRequest.connectionWillClose(msgJson);
            break;
        case 'activeFileSet':
            handleRequest.activeFileSet(msgJson);
            break;
        case 'fileClosed':
            handleRequest.fileClosed(msgJson);
            break;
        case 'operation':
            rt.jobWorker.onReceiveOperation(msgJson, client);
            break;
        default:
            break;
    }
};

var callbackOnClose = function() {
    console.log("Synchronisation controller - connection closed");
};

var handleRequest = {

    initConnection: function(client, msgJson) {

        connectionPool[msgJson.userId] = client;
        utils.addUserToProjectConnections(msgJson.projectId, msgJson.userId);

        var collaboratorsConnected = utils.getCollaboratorsConnected(msgJson.projectId, msgJson.userId);

        var collaboratorsActiveFile = [];
        collaboratorsConnected.forEach(function(item, index) {
            var activeFileJson = {
                userId: item,
                fileId: utils.getCollaboratorActiveFile(item)
            };
            collaboratorsActiveFile.push(activeFileJson);
        });

        var initConnectionJson = {
            action: 'initConnection',
            collaboratorsConnected: collaboratorsConnected,
            collaboratorsActiveFile: collaboratorsActiveFile
        };

        console.log("Synchronisation Controller - Sending back to client: " + JSON.stringify(initConnectionJson));
        client.send(JSON.stringify(initConnectionJson));

        // do a compilation request
        rt.compilation.reqCompilation(msgJson.projectId);

    },

    connectionWillClose: function(msgJson) {
        delete connectionPool[msgJson.userId];
        console.log('connectionWillClose - remove user from file and project connections: ' + msgJson.userId);
        msgJson.openedFilesId.forEach(function(fileId) {
            utils.removeUserFromFileConnections(fileId, msgJson.userId);
        });

        utils.removeUserFromProjectConnections(msgJson.projectId, msgJson.userId);
        utils.setCollaboratorActiveFile(msgJson.projectId, msgJson.userId, undefined);
    },

    activeFileSet: function(msgJson) {
        utils.setCollaboratorActiveFile(msgJson.projectId, msgJson.userId, msgJson.fileId);
    },

    fileClosed: function(msgJson) {
        utils.removeUserFromFileConnections(msgJson.fileId, msgJson.userId);
    }

};

var utils = {

    getCollaboratorsConnected: function(projectId, userId) {
        if (projectConnections[projectId] == undefined) {
            return [];
        }
        
        var collaboratorsConnected = projectConnections[projectId].slice();

        if (collaboratorsConnected.indexOf(userId) != -1 ) { // Need to check because of notifications sent on connectionClose
            collaboratorsConnected.splice(collaboratorsConnected.indexOf(userId), 1);
        }

        return collaboratorsConnected;
    },
    
    getClientConnectionForUserId: function(userId) {
        return connectionPool[userId];
    },

    newFileConnection: function(fileId, userId, fileContent) {

        common.mongoOperations.getProjectElement(fileId
        ).then(function(docs) {

            var operations = docs[0].operations == undefined ? [] : docs[0].operations;
            var server = new ot.Server(fileContent, operations);
            utils.addUserToFileConnections(fileId, userId, server);
        });
    },

    getConnectionForFile: function(fileId) {
        return fileConnections[fileId];
    },

    getConnectionsForProject: function(projectId) {
        return projectConnections[projectId];
    },
    
    addUserToProjectConnections: function(projectId, userId) {
        if (projectConnections[projectId] == undefined) {
            projectConnections[projectId] = [];
        }
        projectConnections[projectId].push(userId);

        if (projectConnections[projectId].length > 1) {
            utils.notifyCollaboratorsOfConnectionUpdate(projectId, userId);
        }
    },

    removeUserFromProjectConnections: function(projectId, userId) {
        if (projectConnections[projectId] == undefined) { return; }
        var index = projectConnections[projectId].indexOf(userId);
        if (index == -1) { return; }
        projectConnections[projectId].splice(index, 1);

        if (projectConnections[projectId].length == 0) {
            delete projectConnections[projectId];
        } else {
            utils.notifyCollaboratorsOfConnectionUpdate(projectId, userId);
        }
    },

    addUserToFileConnections: function(fileId, userId, server) {
        if (fileConnections[fileId] == undefined) {
            var fileConnection = {
                users: [],
                server: server
            };
            fileConnections[fileId] = fileConnection;
        }

        fileConnections[fileId].users.push(userId);
    },

    removeUserFromFileConnections: function(fileId, userId) {
        if (fileConnections[fileId] == undefined) { return; }
        if (fileConnections[fileId].users == undefined) { return; }

        var index = fileConnections[fileId].users.indexOf(userId);

        if (index == -1) { return; }

        fileConnections[fileId].users.splice(index, 1);

        if (fileConnections[fileId].users.length == 0) {
            delete fileConnections[fileId];
        }
    },

    notifyCollaboratorsOfConnectionUpdate: function(projectId, userId) {
        console.log('notifyCollaboratorsOfConnectionUpdate, projectId: ' + projectId + ' userId: ' + userId);

        // All but the user who just connected/disconnected
        var collaboratorsToNotify = utils.getCollaboratorsConnected(projectId, userId);
        console.log('collaborators to notify: ' + JSON.stringify(collaboratorsToNotify));

        collaboratorsToNotify.forEach(function(item, index) {

            // All but this user
            var collaboratorsConnected = utils.getCollaboratorsConnected(projectId, item);

            var updateCollaboratorsJson = {
                action: 'notification',
                type: 'collaboratorsConnected',
                collaboratorsConnected: collaboratorsConnected
            };

            var connection = utils.getClientConnectionForUserId(item);
            if (connection != undefined) {
                console.log("sending " + JSON.stringify(updateCollaboratorsJson) + " to user id: " + item);
                connection.send(JSON.stringify(updateCollaboratorsJson));
            } else {
                console.log("A client has crashed, user id: " + item + " closing up his connection");
                utils.cleanUpConnectionForLostConnection(projectId, item);
            }

        });
    },

    getCollaboratorActiveFile: function(userId) {
        // Can return undefined
        return collaboratorsActiveFile[userId];
    },

    setCollaboratorActiveFile: function(projectId, userId, fileId) {
        if (fileId == undefined) {
            delete collaboratorsActiveFile[userId];
        } else  {
            collaboratorsActiveFile[userId] = fileId;
        }

        utils.notifyCollaboratorsOfActiveFileUpdate(projectId, userId);
    },

    notifyCollaboratorsOfActiveFileUpdate: function(projectId, userId) {

        var collaboratorsToNotify = utils.getCollaboratorsConnected(projectId, userId);

        collaboratorsToNotify.forEach(function(item, index) {

            var activeFileSetJson = {
                action: 'notification',
                type: 'activeFileSet',
                userId: userId,
                fileId: utils.getCollaboratorActiveFile(userId)
            };

            var connection = utils.getClientConnectionForUserId(item);
            if (connection != undefined) {
                console.log("sending " + JSON.stringify(activeFileSetJson) + " to user id: " + item);
                connection.send(JSON.stringify(activeFileSetJson));
            } else {
                console.log("A client has crashed, user id: " + item + " closing up his connection");
                utils.cleanUpConnectionForLostConnection(projectId, item);
            }

        });

    },
    
    cleanUpConnectionForLostConnection: function(projectId, usedId) {
        console.log('user ' + usedId + ' is no longer connected, remove user from file and project connections');
        if (connectionPool[userId] != undefined) {
            delete connectionPool[userId];
        }

        var filesOpened = Object.keys(fileConnections);
        filesOpened.forEach(function (element, index) {
            if (filesOpened[element].indexOf(usedId) != -1) {
                utils.removeUserFromFileConnections(element, usedId);
            }
        });

        utils.removeUserFromProjectConnections(projectId, usedId);
        utils.setCollaboratorActiveFile(projectId, usedId, undefined);
    }

};

exports.initServer = initServer;
exports.utils = utils;